// fetch
// The Fetch API allows you to asynchronously request for a resource (data)
// "Promise" is an object that represents the eventual completion or failure of an asynchronous function and its resulting value
//"fetch" method will return a "promise" that resolves to a "response object"
fetch('https://jsonplaceholder.typicode.com/posts')
// ".json" method from the response object to convert hthe data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from "fetch request"
// .then((json) => console.log(json));
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading
	// prevents default behavior of event
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		// Sets the method of the "Request" object to "POST" following REST API
        // Default method is GET
		method: 'POST',
		// Sets the header data of the "Request" object to be sent to the backend
        // Specified that the content will be in a JSON structure
		headers: {'Content-Type': 'application/json; charset=UTF-8'},
		// Sets the content/body data of the "Request" object to be sent to the backend
        // JSON.stringify converts the object data into a stringified JSON
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');
		// Resets the input fields
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});


// RETRIEVE POSTS
const showPosts = (posts) => {
	// "postEntries" - variable that will contain all the posts   
    let postEntries = "";
       
    posts.forEach((post) => {
    	// "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
    	// "div" - division on where to place the value of title and posts
 	
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    // To check what is stored in the properties of variables
    //console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// EDIT POST

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// UPDATE POST
// Mini-activity
// Create an update post request
// Alert ("Successfuly updated!")
// Reset input fields
// Set attribute disabled true


document.querySelector("#form-edit-post").addEventListener("submit", (e) => {


	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
	
		method: 'PUT',

		headers: {'Content-Type': 'application/json; charset=UTF-8'},

		body: JSON.stringify({
            id: document.querySelector('#txt-edit-id'),
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!');

		document.querySelector('#txt-edit-id').value = null ;
		document.querySelector('#txt-edit-title').value = null ;
		document.querySelector('#txt-edit-body').value = null ;

        document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});


// Activity

// ADD DELETE POST

const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
	
		method: 'DELETE'
	})
    const element =  document.querySelector(`#post-${id}`);
    element.remove();
};
