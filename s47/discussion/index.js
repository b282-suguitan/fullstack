// console.log('Hello world!')


// "document" refers to the whole page
// "querySelector" is used to select a specific object (HTML element) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");


// Alternatively, we can use the getElement functions to retrieve elements
// document.getElementById
// document.getElementByClassName
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFullName = document.getElementById("span-full-name");

// Whenever a user interacts with webpage, this action is considered as an event
// "addEventListener" is a function that takes 2 arguments
// "keydown" is a string identifying an event
// Second argument - will execute once the specified event is triggered
txtFirstName.addEventListener("keyup", (event) => {
    // "innerHTML" property that sets or returns the HTML content
    spanFullName.innerHTML = txtFirstName.value;

    // ".target" contains the element where the event happened
    console.log(event.target);
    // ".value" gets the value of the input object
    console.log(event.target.value);
});