import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} />
    )
}




// ACTIVITY CODE START

// import {Row, Col, Button} from 'react-bootstrap';
// import { useNavigate } from 'react-router-dom';

// export default function NotFound() {
    
//     const navigate = useNavigate();

//     const navigateToHome = () => {
//         navigate('/');
//     };
    
    
//     return (
//         <Row>
//             <Col className="p-5">
//                 <h1>Error 404 - Page Not Found</h1>
//                 <p>The page you are looking for cannot be found.</p>
//                 <Button variant="primary" onClick= {navigateToHome}>Back to Home</Button>
//             </Col>
//         </Row>
//         )
//     }

// ACTIVITY CODE END