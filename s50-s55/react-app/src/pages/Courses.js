// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

import {useState, useEffect} from 'react';

export default function Courses() {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			setCourses(data.map(course => {
				return (
					<CourseCard key={course.id} course = {course} />
				)
			}))
		})
	}, []);

	// console.log(coursesData);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course = {course} />
	// 	)
	// })



	return (
		<>
		{courses}
		</>
	)
};

// courseProp(coursesData[0]);
