import {useState, useEffect} from 'react';


import { Button, Row, Col, Card } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function CourseCard({course}) {

const {name, description, price, _id} = course;

/*
SYNTAX:
const [getter, setter] = useState(initialGetterValue);
*/

// [S50 ACTIVITY]
// const [count, setCount] = useState(0);
// console.log(useState(0));
// const [seats, setSeats] = useState(5);

// function enroll(){
    // if (seats > 0) {
    //     setCount(count + 1);
    //     console.log('Enrollees: ' + count);
    //     setSeats(seats - 1);
    //     console.log('Seats: ' + seats);
    // } else {
    //     alert("No more seats available");
    // };
//     setCount(count + 1);
//     setSeats(seats - 1);
// }

// useEffect(() => {
//     // conditional statement
//     if(seats <= 0) {
//         alert("No more seats available")
//     };
//     // dependency/ies
// }, [seats]);

// // [S50 ACTIVITY END]

return (
    // <Row className="mt-3 mb-3">
    //         <Col xs={12}>
    <Row className="mt-3 mb-3">
    <Col xs={12}>
        <Card className="cardHighlight p-0">
            <Card.Body>
                <Card.Title><h4>{name}</h4></Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    </Col>
</Row> 
    )
}

